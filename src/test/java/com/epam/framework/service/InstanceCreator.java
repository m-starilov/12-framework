package com.epam.framework.service;

import com.epam.framework.model.Instance;

public class InstanceCreator {
    public static final String TESTDATA_NUMBER_OF_INSTANCES = "testdata.instance.numberOfInstances";
    public static final String TESTDATA_OPERATING_SYSTEM_SOFTWARE = "testdata.instance.operatingSystemSoftware";
    public static final String TESTDATA_PROVISIONING_MODEL = "testdata.instance.provisioningModel";
    public static final String TESTDATA_SERIES = "testdata.instance.series";
    public static final String TESTDATA_MACHINE_TYPE = "testdata.instance.machineType";
    public static final String TESTDATA_ADD_GPU = "testdata.instance.addGPU";
    public static final String TESTDATA_GPU_TYPE = "testdata.instance.gpyType";
    public static final String TESTDATA_NUMBER_OF_GPUS = "testdata.instance.numberOfGPUs";
    public static final String TESTDATA_LOCAL_SSD = "testdata.instance.localSSD";
    public static final String TESTDATA_DATACENTER_LOCATION = "testdata.instance.datacenterLocation";
    public static final String TESTDATA_COMMITTED_USAGE = "testdata.instance.committedUsage";

    public static Instance withCredentialsFromProperty() {
        return new Instance(TestDataReader.getTestData(TESTDATA_NUMBER_OF_INSTANCES),
                TestDataReader.getTestData(TESTDATA_OPERATING_SYSTEM_SOFTWARE),
                TestDataReader.getTestData(TESTDATA_PROVISIONING_MODEL),
                TestDataReader.getTestData(TESTDATA_SERIES),
                TestDataReader.getTestData(TESTDATA_MACHINE_TYPE),
                TestDataReader.getTestData(TESTDATA_GPU_TYPE),
                TestDataReader.getTestData(TESTDATA_NUMBER_OF_GPUS),
                TestDataReader.getTestData(TESTDATA_LOCAL_SSD),
                TestDataReader.getTestData(TESTDATA_DATACENTER_LOCATION),
                TestDataReader.getTestData(TESTDATA_COMMITTED_USAGE)
        );
    }

}
