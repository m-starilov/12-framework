package com.epam.framework.model;

import java.util.Objects;

public class Instance {
    private String numberOfInstances;
    private String operatingSystemSoftware;
    private String provisioningModel;
    private String series;
    private String machineType;
    private String gpyType;
    private String numberOfGPUs;
    private String localSSD;
    private String datacenterLocation;
    private String committedUsage;

    public Instance(String numberOfInstances,
                    String operatingSystemSoftware,
                    String provisioningModel,
                    String series,
                    String machineType,
                    String gpyType,
                    String numberOfGPUs,
                    String localSSD,
                    String datacenterLocation,
                    String committedUsage) {
        this.numberOfInstances = numberOfInstances;
        this.operatingSystemSoftware = operatingSystemSoftware;
        this.provisioningModel = provisioningModel;
        this.series = series;
        this.machineType = machineType;
        this.gpyType = gpyType;
        this.numberOfGPUs = numberOfGPUs;
        this.localSSD = localSSD;
        this.datacenterLocation = datacenterLocation;
        this.committedUsage = committedUsage;
    }

    public String getNumberOfInstances() {
        return numberOfInstances;
    }

    public void setNumberOfInstances(String numberOfInstances) {
        this.numberOfInstances = numberOfInstances;
    }

    public String getOperatingSystemSoftware() {
        return operatingSystemSoftware;
    }

    public void setOperatingSystemSoftware(String operatingSystemSoftware) {
        this.operatingSystemSoftware = operatingSystemSoftware;
    }

    public String getProvisioningModel() {
        return provisioningModel;
    }

    public void setProvisioningModel(String provisioningModel) {
        this.provisioningModel = provisioningModel;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getMachineType() {
        return machineType;
    }

    public void setMachineType(String machineType) {
        this.machineType = machineType;
    }

    public String getGpyType() {
        return gpyType;
    }

    public void setGpyType(String gpyType) {
        this.gpyType = gpyType;
    }

    public String getNumberOfGPUs() {
        return numberOfGPUs;
    }

    public void setNumberOfGPUs(String numberOfGPUs) {
        this.numberOfGPUs = numberOfGPUs;
    }

    public String getLocalSSD() {
        return localSSD;
    }

    public void setLocalSSD(String localSSD) {
        this.localSSD = localSSD;
    }

    public String getDatacenterLocation() {
        return datacenterLocation;
    }

    public void setDatacenterLocation(String datacenterLocation) {
        this.datacenterLocation = datacenterLocation;
    }

    public String getCommittedUsage() {
        return committedUsage;
    }

    public void setCommittedUsage(String committedUsage) {
        this.committedUsage = committedUsage;
    }

    @Override
    public String toString() {
        return "Instance{" +
                "numberOfInstances=" + numberOfInstances +
                ", operatingSystemSoftware='" + operatingSystemSoftware + '\'' +
                ", provisioningModel='" + provisioningModel + '\'' +
                ", series='" + series + '\'' +
                ", machineType='" + machineType + '\'' +
                ", gpyType='" + gpyType + '\'' +
                ", numberOfGPUs=" + numberOfGPUs +
                ", localSSD='" + localSSD + '\'' +
                ", datacenterLocation='" + datacenterLocation + '\'' +
                ", committedUsage='" + committedUsage + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Instance instance = (Instance) o;
        return Objects.equals(numberOfInstances, instance.numberOfInstances) && Objects.equals(operatingSystemSoftware, instance.operatingSystemSoftware) && Objects.equals(provisioningModel, instance.provisioningModel) && Objects.equals(series, instance.series) && Objects.equals(machineType, instance.machineType) && Objects.equals(gpyType, instance.gpyType) && Objects.equals(numberOfGPUs, instance.numberOfGPUs) && Objects.equals(localSSD, instance.localSSD) && Objects.equals(datacenterLocation, instance.datacenterLocation) && Objects.equals(committedUsage, instance.committedUsage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numberOfInstances, operatingSystemSoftware, provisioningModel, series, machineType, gpyType, numberOfGPUs, localSSD, datacenterLocation, committedUsage);
    }
}
