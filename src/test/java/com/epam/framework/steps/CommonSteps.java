package com.epam.framework.steps;

import com.epam.framework.driver.DriverSingleton;
import com.epam.framework.page.PricingCalculatorPage;

public class CommonSteps {

    private final PricingCalculatorPage pricingCalculatorPage = new PricingCalculatorPage(DriverSingleton.getDriver());

    public void fillOutForm() {
        pricingCalculatorPage.inputNumberOfInstances();
        pricingCalculatorPage.setOperatingSystemSoftware();
        pricingCalculatorPage.setProvisioningModel();
        pricingCalculatorPage.setSeries();
        pricingCalculatorPage.setMachineType();
        pricingCalculatorPage.setAddGPUsTrue();
        pricingCalculatorPage.setGpyType();
        pricingCalculatorPage.setNumberOfGPUs();
        pricingCalculatorPage.setLocalSSD();
        pricingCalculatorPage.setDatacenterLocation();
        pricingCalculatorPage.setCommittedUsage();
    }
}
