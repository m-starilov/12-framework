package com.epam.framework.util;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class StringUtils {
    public static String getTextNode(WebElement e) {
        String text = e.getText().trim();
        List<WebElement> children = e.findElements(By.xpath("./*"));
        for (WebElement child : children) {
            text = text.replaceFirst(child.getText(), "").trim();
        }
        return text;
    }

    public static String getCost(String s) {
        return s.replaceAll("per 1 month", "")
                .replaceAll("[a-zA-Z ]|:", "");
    }
}
