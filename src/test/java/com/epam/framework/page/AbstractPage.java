package com.epam.framework.page;

import org.openqa.selenium.WebDriver;

public abstract class AbstractPage {
    protected WebDriver driver;

    protected abstract void openPage();

    protected AbstractPage(WebDriver driver) {
        this.driver = driver;
    }
}
