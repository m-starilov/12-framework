package com.epam.framework.page;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static com.epam.framework.util.StringUtils.getCost;

public class TempMailMailboxPage {

    public WebDriver driver;
    private final Logger logger = LogManager.getRootLogger();
    private final String estimatedMonthlyCostXpath = "//*[contains(text(),'Estimated Monthly Cost:')]";
    @FindBy(xpath = estimatedMonthlyCostXpath)
    private WebElement estimatedMonthlyCost;

    public TempMailMailboxPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public String getEstimatedMonthlyCost() {
        for (int i = 0; i < 3; i++) {
            driver.switchTo().frame("ifmail");
            if (driver.findElements(By.xpath(estimatedMonthlyCostXpath)).size() > 0) {
                logger.info("Founded element with xpath " + estimatedMonthlyCostXpath);
                break;
            } else {
                driver.navigate().refresh();
                logger.info("Not founded element with xpath " + estimatedMonthlyCostXpath);
                logger.info("Refreshed page");
            }
        }
        logger.info("Getting total estimated cost " + estimatedMonthlyCost.getText());
        return getCost(estimatedMonthlyCost.getText());
    }

}
