package com.epam.framework.page;

import com.epam.framework.util.PageHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static com.epam.framework.util.PageHelper.waitForElementClickable;

public class SearchResultsPage {
    public WebDriver driver;
    private final Logger logger = LogManager.getRootLogger();
    @FindBy(xpath = "//div[@class = 'gs-title']/a[b = 'Google Cloud Pricing Calculator']")
    private WebElement resultLink;

    public SearchResultsPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public PricingCalculatorPage resultClick() {
        PageHelper.waitForElementClickable(resultLink).click();
        logger.info("Found and clicked link with text: [Google Cloud Pricing Calculator]");
        return new PricingCalculatorPage(driver);
    }

}
