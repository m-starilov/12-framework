package com.epam.framework.page;

import com.epam.framework.model.Instance;
import com.epam.framework.service.InstanceCreator;
import com.epam.framework.util.PageHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static com.epam.framework.util.PageHelper.scrollIntoView;
import static com.epam.framework.util.PageHelper.waitForElementClickable;
import static com.epam.framework.util.StringUtils.getCost;

public class PricingCalculatorPage {
    private WebDriver driver;
    private final Logger logger = LogManager.getRootLogger();
    private final Instance instance = InstanceCreator.withCredentialsFromProperty();
    private final static String COMMON_DROPDOWN_XPATH = "//div[@class[contains(.,'md-active')]]//md-option[@value='%s']";
    @FindBy(xpath = "//md-tab-item/div[@title='Compute Engine']")
    private WebElement computeEngineIcon;
    @FindBy(id = "input_85")
    private WebElement numberOfInstances;
    @FindBy(xpath = "//md-select[@ng-model='listingCtrl.computeServer.os']")
    private WebElement operatingSystemSoftware;
    @FindBy(xpath = "//form[@name='ComputeEngineForm']//md-select[@placeholder = 'VM Class']")
    private WebElement provisioningModel;
    @FindBy(xpath = "//form[@name='ComputeEngineForm']//md-select[@placeholder = 'Series']")
    private WebElement series;
    @FindBy(xpath = "//form[@name='ComputeEngineForm']//md-select[@placeholder = 'Instance type']")
    private WebElement machineType;
    @FindBy(xpath = "//md-checkbox[@ng-model='listingCtrl.computeServer.addGPUs']")
    private WebElement addGPUsCheckbox;
    @FindBy(xpath = "//form[@name='ComputeEngineForm']//md-select[@placeholder = 'GPU type']")
    private WebElement gpyType;
    @FindBy(xpath = "//md-select[@placeholder = 'Number of GPUs']")
    private WebElement numberOfGPUs;
    @FindBy(xpath = "//form[@name='ComputeEngineForm']//md-select[@placeholder = 'Local SSD']")
    private WebElement localSSD;
    @FindBy(xpath = "//md-select[@placeholder = 'Datacenter location']")
    private WebElement datacenterLocation;
    @FindBy(xpath = "//form[@name='ComputeEngineForm']//md-select[@placeholder = 'Committed usage']")
    private WebElement committedUsage;
    @FindBy(xpath = "//form[@name='ComputeEngineForm']//button[contains(text(),'Add to Estimate')]")
    private WebElement addToEstimateButton;
    @FindBy(xpath = "//button[contains(text(),'Email')]")
    private WebElement emailButton;
    @FindBy(xpath = "//input[@name = 'description' and @type = 'email']")
    private WebElement emailInput;
    @FindBy(xpath = "//button[contains(text(),'Send Email')]")
    private WebElement sendEmailButton;
    @FindBy(xpath = "//*[contains(text(), 'Total Estimated Cost')]")
    private WebElement totalEstimatedCost;
    @FindBy(xpath = "//iframe[@src[contains(.,'/products/calculator/')]]")
    private WebElement iframe;
    @FindBy(xpath = "//iframe[@id='myFrame']")
    private WebElement myFrame;

    public PricingCalculatorPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    private void switchToFrame() {
        driver.switchTo().frame(iframe).switchTo().frame(myFrame);
    }

    public void pickComputeEngineSection() {
        switchToFrame();
        logger.info("WebDriver switched on frame [" + iframe + "]");
        logger.info("WebDriver switched on frame [" + myFrame + "]");
        computeEngineIcon.click();
        logger.info("Clicked the computeEngineIcon with xpath [//md-tab-item/div[@title='Compute Engine']]");
    }

    public void inputNumberOfInstances() {
        numberOfInstances.click();
        numberOfInstances.sendKeys(instance.getNumberOfInstances());
        logger.info("numberOfInstances - " + instance.getNumberOfInstances());
    }

    public void setOperatingSystemSoftware() {
        operatingSystemSoftware.click();
        scrollIntoView(numberOfInstances);
        getElement(instance.getOperatingSystemSoftware()).click();
        writeToLog(operatingSystemSoftware);
    }

    public void setProvisioningModel() {
        provisioningModel.click();
        getElement(instance.getProvisioningModel()).click();
        writeToLog(provisioningModel);
    }

    public void setSeries() {
        series.click();
        getElement(instance.getSeries()).click();
        writeToLog(series);
    }

    public void setMachineType() {
        machineType.click();
        scrollIntoView(numberOfInstances);
        getElement(instance.getMachineType()).click();
        writeToLog(machineType);
    }

    public void setAddGPUsTrue() {
        addGPUsCheckbox.click();
        logger.info("addGPUsCheckbox - True");
    }

    public void setGpyType() {
        scrollIntoView(series);
        gpyType.click();
        getElement(instance.getGpyType()).click();
        writeToLog(gpyType);
    }

    public void setNumberOfGPUs() {
        numberOfGPUs.click();
        getElement(instance.getNumberOfGPUs()).click();
        writeToLog(numberOfGPUs);
    }

    public void setLocalSSD() {
        localSSD.click();
        scrollIntoView(addGPUsCheckbox);
        getElement(instance.getLocalSSD()).click();
        writeToLog(localSSD);
    }

    public void setDatacenterLocation() {
        datacenterLocation.click();
        getElement(instance.getDatacenterLocation()).click();
        writeToLog(datacenterLocation);
    }

    public void setCommittedUsage() {
        committedUsage.click();
        getElement(instance.getCommittedUsage()).click();
        writeToLog(committedUsage);
    }

    public void addToEstimate() {
        scrollIntoView(localSSD);
        addToEstimateButton.click();
        logger.info("Clicked the addToEstimateButton" + addToEstimateButton + "");
    }

    public void emailEstimate() {
        emailButton.click();
        logger.info("Clicked the emailButton " + emailButton + "");
    }

    public void pastAndSendEmail() {
        switchToFrame();
        emailInput.click();
        emailInput.sendKeys(Keys.chord(Keys.CONTROL, "v"));
        scrollIntoView(emailInput);
        PageHelper.waitForElementClickable(sendEmailButton).click();
        logger.info("Pasted email and sent");
    }

    public String getTotalEstimatedCost() {
        logger.info("Getting total estimated cost " + totalEstimatedCost.getText());
        return getCost(totalEstimatedCost.getText());
    }

    private WebElement getElement(String value) {
        return waitForElementClickable(String.format(COMMON_DROPDOWN_XPATH, value));
    }

    private void writeToLog(WebElement element) {
        logger.info(element.toString() + " - " + element.getText());
    }
}
