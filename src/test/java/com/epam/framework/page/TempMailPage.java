package com.epam.framework.page;

import com.epam.framework.util.PageHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TempMailPage extends AbstractPage {

    private static final String URL = "https://yopmail.com/en/email-generator";
    private final Logger logger = LogManager.getRootLogger();
    @FindBy(xpath = "//button[@id='cprnd']")
    private WebElement copyButton;
    @FindBy(xpath = "//span[contains(text(),'Check Inbox')]")
    private WebElement checkInboxButton;


    public TempMailPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @Override
    public void openPage() {
        driver.switchTo().newWindow(WindowType.TAB).get(URL);
        logger.info("Opened site by URL: [" + URL + "] in new tab");
    }

    public void copyToClipBoard() {
        PageHelper.waitForElementClickable(copyButton).click();
        logger.info("Clicked the copyButton " + copyButton);
    }

    public void checkInbox() {
        PageHelper.waitForElementClickable(checkInboxButton).click();
        logger.info("Clicked the checkInboxButton " + checkInboxButton);
        new TempMailMailboxPage(driver);
    }

}
