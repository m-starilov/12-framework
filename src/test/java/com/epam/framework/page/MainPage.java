package com.epam.framework.page;

import com.epam.framework.util.PageHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainPage extends AbstractPage {
    private static final String URL = "https://cloud.google.com/";
    private final Logger logger = LogManager.getRootLogger();
    @FindBy(xpath = "//input[@name='q']")
    private WebElement searchBox;

    public MainPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @Override
    public void openPage() {
        driver.get(URL);
        logger.info("Opened site by URL: [" + URL + "] in max window size");
    }

    public void searchPricingCalculator() {
        PageHelper.waitForElementClickable(searchBox).click();
        searchBox.sendKeys("Google Cloud Platform Pricing Calculator");
        searchBox.sendKeys(Keys.ENTER);
        logger.info("Sent search request: [Google Cloud Platform Pricing Calculator]");
    }

}
