package com.epam.framework.test;

import com.epam.framework.page.*;
import com.epam.framework.steps.CommonSteps;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.epam.framework.util.PageHelper.switchTab;

public class EmailEstimateOfComputeEngineTest extends CommonConditions {
    private CommonSteps commonSteps;

    @Test(description = "Task 4. Hardcore")
    public void shouldAddToEstimateComputeEngineCalculation() {
        MainPage mainPage = new MainPage(driver);
        mainPage.openPage();
        mainPage.searchPricingCalculator();
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver);
        searchResultsPage.resultClick();
        PricingCalculatorPage pricingCalculatorPage = new PricingCalculatorPage(driver);
        pricingCalculatorPage.pickComputeEngineSection();
        commonSteps.fillOutForm();
        pricingCalculatorPage.addToEstimate();
        pricingCalculatorPage.emailEstimate();
        TempMailPage tempMailPage = new TempMailPage(driver);
        tempMailPage.openPage();
        tempMailPage.copyToClipBoard();
        switchTab();
        pricingCalculatorPage.pastAndSendEmail();
        String siteCost = pricingCalculatorPage.getTotalEstimatedCost();
        switchTab();
        tempMailPage.checkInbox();
        String mailCost = new TempMailMailboxPage(driver).getEstimatedMonthlyCost();

        Assert.assertEquals(mailCost, siteCost);
    }

}
